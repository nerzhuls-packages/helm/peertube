module gitlab.com/nerzhuls-packages/helm/postgres-backup/test

go 1.16

require (
	github.com/gruntwork-io/terratest v0.38.2
	github.com/stretchr/testify v1.7.0
	k8s.io/api v0.20.6
)
