# v1.2.1

## Fix
 - Adjust initialDelaySecond for startupProbe to avoid waiting 5min even if the instance starts quickly
 - Adjust startupProbe port to use 9000 instead of http 

# v1.2.0

## Features
 - Compatibility with helm 3 and k8s 1.16+
 - Now use StartupProbe to enable very long start when updating Peertube and running upgrade scripts

# v1.1.2

## Fixes
 - Update compatibility to Peertube v2.3.0
 - Remove deprecated env variables for docker

# v1.1.1

Ensure compatibility with Peertube v2.2.0 by removing the PEERTUBE_TRUST_PROXY env variable that does not exist anymore.

Is you used it, configure your production.yml instead for better reliability.

# v1.1.0

## Features  

Add a nginx sidecar to use the official Peertube optimisations for Nginx (Fix #2)

# v1.0.2

## Breaking changes

The option initcontainer is now renamed chowncontainer as it fit better to its usage.

## Fixes

 - Fix issue #3 to avoid breaking Peertube upgrade by using 2 initContainers

# v1.0.1

Now use Peertube v2.1.0-buster as default

# v1.0.0

*Initial release*

##Features

 - Deploy Peertube in kubernetes
 - Deploy redis server to be used by Peertube
 - Allow to enable usual docker "chown" to speed up startup through InitContainer


